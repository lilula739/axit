document.addEventListener("DOMContentLoaded", ready);

function ready() {
  const tabsContainer = document.querySelector(".tabs__cubes");
  const tabs = document.querySelectorAll('[data-tab-cube="true"]');
  const slides = document.querySelectorAll('[data-tab-slide="true"]');

  if (tabs.length) {
    tabs[0].classList.add("active");
  }
  if (slides.length) {
    slides[0].classList.add("active");
  }

  function tabsClickHandler(event) {
    const targetElement = event.target;

    const clickedTab = targetElement.dataset["tab-cube"]
      ? targetElement.dataset["tab-cube"]
      : targetElement.closest('[data-tab-cube="true"]');

    if (clickedTab) {
      tabs.forEach((tab, index) => {
        tab.classList.remove("active");

        if (slides[index]) {
          if (clickedTab === tab) {
            slides[index].classList.add("active");
          } else {
            slides[index].classList.remove("active");
          }
        }
      });
      clickedTab.classList.add("active");
    }
  }

  tabsContainer.addEventListener("click", tabsClickHandler);
}
